package io.training.api.controllers;

import akka.actor.ActorSystem;
import akka.stream.Materializer;
import com.google.inject.Inject;
import com.mongodb.client.ListIndexesIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import io.training.api.actors.MongoMonitoringActor;
import io.training.api.models.Transaction;
import io.training.api.mongo.IMongoDB;
import org.bson.Document;
import org.bson.conversions.Bson;
import play.Logger;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.libs.streams.ActorFlow;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LectureTenController extends Controller {
	@Inject
	IMongoDB mongoDB;
	@Inject
	private ActorSystem actorSystem;
	@Inject
	private Materializer materializer;
	@Inject
	HttpExecutionContext ec;

	public WebSocket watcher () {
		return WebSocket.Text.accept(request -> ActorFlow.actorRef((out) -> MongoMonitoringActor.props(out, mongoDB, ec.current()), actorSystem, materializer));
	}


	public Result count () {
		MongoCollection<Transaction> collection = mongoDB.getMongoDatabase()
				.getCollection("transactions", Transaction.class);

		List<Bson> pipeline = new ArrayList<>();
		pipeline.add(Aggregates.match(Filters.eq("categoryName", "Instore Services")));
		pipeline.add(Aggregates.group(new Document("categoryName", "$categoryName"),
				Accumulators.sum("total", "$salesIncVatActual")));
		pipeline.add(Aggregates.project(Projections.fields(
				Projections.computed("category", "$_id.categoryName"),
				Projections.include("total"),
				Projections.exclude("_id")
		)));
		List<Document> aggregated = collection
				.aggregate(pipeline, Document.class)
				.into(new ArrayList<>());
		return ok(Json.toJson(aggregated));
	}


	public Result dropIndexes () {
		MongoCollection<Transaction> collection = mongoDB.getMongoDatabase()
				.getCollection("transactions", Transaction.class);
		collection.dropIndexes();
		return ok(Json.toJson("Deleted Index"));
	}


	public Result createIndexes () {
		MongoCollection<Transaction> collection = mongoDB.getMongoDatabase()
				.getCollection("transactions", Transaction.class);
		collection.createIndex(Indexes.ascending("categoryName"));
		return ok(Json.toJson("Created Index"));
	}
}