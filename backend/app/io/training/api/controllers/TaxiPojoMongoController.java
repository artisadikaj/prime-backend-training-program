package io.training.api.controllers;

import com.google.inject.Inject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import io.training.api.models.Taxi;
import io.training.api.mongo.IMongoDB;
import org.bson.types.ObjectId;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaxiPojoMongoController extends Controller {

	@Inject
	IMongoDB mongoDB;

	/**
	 * Retreives all taxis from the pojo taxi collection
	 * @param request
	 * @return
	 */
	public Result all (Http.Request request) {
		MongoCollection<Taxi> collection = mongoDB.getMongoDatabase().getCollection("pojo", Taxi.class);
		List<Taxi> taxis = collection.find().into(new ArrayList<>());
		return ok(Json.toJson(taxis));
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result save (Http.Request request) {
		Optional<Taxi> node = request.body().parseJson(Taxi.class);
		if (!node.isPresent()) {
			return badRequest(Json.toJson("ooo no no"));
		}
		Taxi item = node.get();
		item.setId(new ObjectId());
		MongoCollection<Taxi> collection = mongoDB.getMongoDatabase().getCollection("pojo", Taxi.class);
		collection.insertOne(item);
		return ok(Json.toJson(item));
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result update (Http.Request request, String id) {
		Optional<Taxi> node = request.body().parseJson(Taxi.class);
		if (!node.isPresent()) {
			return badRequest(Json.toJson("ooo no no"));
		}
		Taxi item = node.get();
		MongoCollection<Taxi> collection = mongoDB.getMongoDatabase().getCollection("pojo", Taxi.class);
//		collection.replaceOne(Filters.eq("_id", item.getId()), item);
		collection.replaceOne(Filters.eq("_id", new ObjectId(id)), item);
		return ok(Json.toJson(item));
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result delete (Http.Request request, String id) {
		Optional<Taxi> node = request.body().parseJson(Taxi.class);
		if (!node.isPresent()) {
			return badRequest(Json.toJson("ooo no no"));
		}
		Taxi item = node.get();
		MongoCollection<Taxi> collection = mongoDB.getMongoDatabase().getCollection("pojo", Taxi.class);
//		collection.deleteOne(Filters.eq("_id", item.getId()));
		collection.deleteOne(Filters.eq("_id", new ObjectId(id)));
		return ok(Json.toJson(item));
	}

}