import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created by agonlohaj on 06 Oct, 2020
 */
public class SimpleTest {

	@Test
	public void testSum() {
		int a = 1 + 1;
		assertEquals(2, a);
	}

	@Test
	public void testString() {
		String str = "Hello world";
		assertFalse(str.isEmpty());
	}
}
