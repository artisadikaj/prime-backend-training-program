import { PAGES } from 'Constants'

const routes = [
  {
    display: 'Home',
    id: PAGES.HOME,
  },
  {
    display: '1. Setup and Introduction',
    id: PAGES.LECTURE_1.ID,
    children: [
      {
        display: 'Getting Started',
        id: PAGES.LECTURE_1.GETTING_STARTED,
        children: [
          {
            display: 'GitLab',
          },
          {
            display: 'Source Tree'
          },
          {
            display: 'SBT (Scala Build Tool)'
          },
          {
            display: 'IntelliJ or VS Code'
          },
          {
            display: 'Slack'
          },
          {
            display: 'MongoDB'
          },
          {
            display: 'NPM'
          },
          {
            display: 'Postman'
          },
          {
            display: 'Redis'
          }
        ]
      },
      {
        display: 'Project Setup',
        id: PAGES.LECTURE_1.PROJECT_SETTUP,
        children: [
          {
            display: 'Seting up the repository'
          },
          {
            display: 'Running the Application'
          }
        ]
      },
      {
        display: 'Agile Methodology',
        id: PAGES.LECTURE_1.AGILE_METHODOLOGY,
        children: [
          {
            display: 'Scrum'
          }
        ]
      },
      {
        display: 'Working with GIT (Source Tree or Terminal)',
        id: PAGES.LECTURE_1.WORKING_WITH_GIT,
        children: [
          {
            display: 'Workflow'
          },
          {
            display: 'Add & Commit'
          },
          {
            display: 'Pushing Changes'
          },
          {
            display: 'Branching'
          },
          {
            display: 'Update and Merge'
          }
        ]
      },
      {
        display: 'Way of Working',
        id: PAGES.LECTURE_1.WAY_OF_WORKING,
        children: [
          {
            display: 'GitLab Setup',
          },
          {
            display: 'Rules',
          },
          {
            id: 'way_of_working_exercise_1',
            display: 'Exercise 1'
          }
        ]
      }
    ]
  },
  {
    display: '2. Java Recap',
    id: PAGES.LECTURE_2.ID,
    children: [
      {
        display: 'Java Programming Language',
        id: PAGES.LECTURE_2.JAVA_INTRODUCTION,
        children: [
          {
            display: 'Introduction',
          },
          {
            display: 'Syntax'
          },
          {
            display: 'Java Variable'
          },
          {
            display: 'Data Types'
          },
          {
            display: 'Strongly Typed'
          },
          {
            display: 'Pass by Value'
          }
        ]
      },
      {
        display: 'Object Oriented Programming',
        id: PAGES.LECTURE_2.OOP,
        children: [
          {
            display: 'A Class in Java',
          },
          {
            display: 'Objects',
          },
          {
            display: 'Java Inheritance',
          },
          {
            display: 'Java Polymorphism',
          },
          {
            display: 'Java Encapsulation',
          },
          {
            display: 'Java Abstraction',
          }
        ]
      },
      {
        display: 'Object Oriented Programming Misc',
        id: PAGES.LECTURE_2.OOP_MISC,
        children: [
          {
            display: 'Keywords',
          },
          {
            display: 'Access Modifiers'
          },
          {
            display: 'Object Types and Reference Types',
          }
        ]
      },
      {
        display: 'Java 8+ Features',
        id: PAGES.LECTURE_2.JAVA_RELEASES,
        children: [
          {
            display: 'Lambda Expressions',
          },
          {
            display: 'Java Streaming API',
          },
          {
            display: 'Completable Futures',
          }
        ]
      },
      {
        display: 'Assignments',
        id: PAGES.LECTURE_2.ASSIGNMENTS,
        children: [
          {
            display: 'Graph Functions'
          },
          {
            display: 'Binary Search Sort'
          }
        ]
      }
    ]
  },
  {
    display: '3. Play Framework Introduction',
    id: PAGES.LECTURE_3.ID,
    children: [
      {
        display: 'Play Framework',
        id: PAGES.LECTURE_3.PLAY_INTRODUCTION,
        children: [
          {
            id: 'play_intro',
            display: 'Introduction'
          },
          {
            display: 'Application Overview'
          },
          {
            display: 'Exploring the Project'
          },
          {
            id: "action_controllers_responses",
            display: 'Actions Controllers and Responses'
          }
        ]
      },
      {
        display: 'Rest API',
        id: PAGES.LECTURE_3.REST_API,
        children: []
      }
    ]
  },
  {
    display: '4. Routing, Requests and Responses',
    id: PAGES.LECTURE_4.ID,
    children: [
      {
        display: 'Play Routing',
        id: PAGES.LECTURE_4.PLAY_ROUTING,
        children: [
          {
            display: 'The built-in HTTP router'
          },
          {
            display: 'Dependency Injection'
          },
          {
            display: 'The routes file syntax'
          },
          {
            display: 'The HTTP method'
          },
          {
            display: 'The URI pattern'
          },
          {
            display: 'Call to action generator method'
          },
          {
            display: 'Routing priority'
          },
          {
            display: 'Reverse routing'
          }
        ]
      },
      {
        display: 'Manipulating Responses',
        id: PAGES.LECTURE_4.MANIPULATING_RESPONSES,
        children: [
          {
            display: 'Changing the Default Content Type'
          },
          {
            display: 'Manipulating HTTP headers'
          },
          {
            display: 'Setting and discarding cookies'
          },
          {
            display: 'Changing the charset for text based HTTP responses'
          }
        ]
      },
      {
        display: 'Body Parsers',
        id: PAGES.LECTURE_4.BODY_PARSERS,
        children: [
          {
            display: 'What is a body parser?'
          },
          {
            display: 'Using the built in body parsers'
          },
          {
            display: 'Writing a custom body parser'
          }
        ]
      },
      {
        display: 'Handling and serving JSON',
        id: PAGES.LECTURE_4.HANDLING_SERVING_JSON,
        children: [
          {
            display: 'Mapping Java objects to JSON'
          },
          {
            display: 'Handling a JSON request'
          },
          {
            display: 'Serving a JSON response'
          },
          {
            display: 'Advanced Usages'
          },
        ]
      },
      {
        display: 'Assignments',
        id: PAGES.LECTURE_4.ASSIGNMENTS,
        children: [
          {
            display: 'Routes'
          },
          {
            display: 'Binary Tree'
          }
        ]
      }
    ]
  },
  {
    display: '5. Actions, Files and Async',
    id: PAGES.LECTURE_5.ID,
    children: [
      {
        display: 'File Uploads',
        id: PAGES.LECTURE_5.FILE_UPLOADS,
        children: [
          {
            display: 'Uploading files in a form using multipart/form-data'
          },
          {
            display: 'Direct file upload',
          },
          {
            display: 'Cleaning up temporary files'
          }
        ]
      },
      {
        display: 'Handling asynchronous results',
        id: PAGES.LECTURE_5.PLAY_ASYNC,
        children: [
          {
            display: 'Make controllers asynchronous',
          },
          {
            display: 'Creating non-blocking actions',
          },
          {
            display: 'How to create a CompletionStage<Result>',
          },
          {
            display: 'Using CustomExecutionContext and HttpExecution'
          },
          {
            display: 'Actions are asynchronous by default'
          },
          {
            display: 'Handling time-outs'
          }
        ]
      },
      {
        display: 'Action Composition',
        id: PAGES.LECTURE_5.ACTION_COMPOSITION,
        children: [
          {
            display: 'Reminder about actions'
          },
          {
            display: 'Composing actions'
          },
          {
            display: 'Defining custom action annotations'
          },
          {
            display: 'Annotating controllers'
          },
          {
            display: 'Passing objects from action to controller'
          },
          {
            display: 'Debugging the action composition order'
          },
          {
            display: 'Using Dependency Injection'
          }
        ]
      }
    ]
  },
  {
    display: '6. Validation, Injection and Redis',
    id: PAGES.LECTURE_6.ID,
    children: [
      {
        display: 'Data Validation',
        id: PAGES.LECTURE_6.VALIDATION,
        children: [
          {
            display: 'Data Quality'
          },
          {
            display: 'Data Validation using plain Java'
          },
          {
            display: 'Validation using Hibernate'
          },
          {
            display: 'Creating a simple constraint'
          },
          {
            display: 'Constraint Composition'
          }
        ]
      },
      {
        display: 'Dependency Injection',
        id: PAGES.LECTURE_6.DEPENDENCY_INJECTION,
        children: [
          {
            display: 'Motivation'
          },
          {
            display: 'Declaring dependencies'
          },
          {
            display: 'Dependency injecting controllers',
          },
          {
            display: 'Component lifecycle',
          },
          {
            display: 'Singletons'
          },
          {
            display: 'Stopping/cleaning up'
          },
          {
            display: 'Providing custom bindings',
          },
          {
            display: 'Managing circular dependencies'
          }
        ]
      },
      {
        display: 'Cache API on Redis',
        id: PAGES.LECTURE_6.REDIS_CACHING,
        children: [
          {
            display: 'Provided APIs'
          }
        ]
      },
      {
        display: 'Assignments',
        id: PAGES.LECTURE_6.ASSIGNMENTS,
        children: [
          {
            display: 'Routes'
          }
        ]
      }
    ]
  },
  {
    display: '7. Introduction to Akka',
    id: PAGES.LECTURE_7.ID,
    children: [,
      {
        display: 'Akka',
        id: PAGES.LECTURE_7.INTRODUCTION,
        children: [
          { display: 'Introduction' },
          { display: 'Motivation' },
          { display: 'Akka Model' },
          { display: 'Akka Libraries and Modules' },
        ]
      },
      {
        display: 'Akka and Play',
        id: PAGES.LECTURE_7.AKKA_PLAY,
        children: [
          { display: 'The application actor system' },
          { display: 'Dependency injecting actors' },
          { display: 'Configuration' },
          { display: 'Executing a block of code asynchronously' },
          { display: 'Akka Coordinated Shutdown' },
          { display: 'Updating Akka version' }
        ]
      },
      {
        display: 'Assignments',
        id: PAGES.LECTURE_7.ASSIGNMENTS,
        children: [
          {
            display: 'Routes'
          }
        ]
      }
    ]
  },
  {
    display: '8. Akka - Distributed',
    id: PAGES.LECTURE_8.ID,
    children: [,
      {
        display: 'Akka Cluster',
        id: PAGES.LECTURE_8.AKKA_CLUSTER,
        children: [
          { display: 'Getting Started' },
          { display: 'Cluster Specification' },
          { display: 'Cluster Membership Service' },
          { display: 'Serialization' },
        ]
      },
      {
        display: 'Distributed Publish Subscribe',
        id: PAGES.LECTURE_8.AKKA_PUB_SUB,
        children: [
          { display: 'Introduction' },
          { display: 'Publish' },
          { display: 'Send' },
          { display: 'Playground', id: 'section_eight_playground' }
        ]
      },
    ],
  },
  {
    display: '9. Mongo Database',
    id: PAGES.LECTURE_9.ID,
    children: [,
      {
        display: 'Introduction',
        id: PAGES.LECTURE_9.MONGO_DB,
        children: [
          { display: 'How it works' }
        ]
      },
      {
        display: 'Usage with Java',
        id: PAGES.LECTURE_9.MONGO_SIMPLE_QUERIES,
        children: [
          { display: 'POJOs - Plain Old Java Objects' },
          { display: 'Filters' },
          { display: 'Sorting' },
          { display: 'CRUD' },
        ]
      },
      {
        display: 'Aggregations',
        id: PAGES.LECTURE_9.AGGREGATIONS,
        children: [
          { display: 'Aggregation Pipeline' },
          { display: 'Single Purpose Aggregation' },
          { display: 'Exercises' }
        ]

      },
      {
        display: 'Assignments',
        id: PAGES.LECTURE_9.ASSIGNMENTS,
        children: [
          { display: 'Routes', id: 'assignment_nine_routes' }
        ]
      },
    ],
  },
  {
    display: '10. Advanced Mongo Topics',
    id: PAGES.LECTURE_10.ID,
    children: [
      {
        display: 'Transactions',
        id: PAGES.LECTURE_10.TRANSACTIONS,
      },
      {
        display: 'Change Streams',
        id: PAGES.LECTURE_10.CHANGE_STREAMS,
      },
      {
        display: 'Indexing',
        id: PAGES.LECTURE_10.INDEXING,
        children: [
          { display: 'Default _id'},
          { display: 'Ascending vs Descending' },
          { display: 'Types of Indexes' },
          { display: 'Example', id: 'lecture9_example' },
        ]
      },
      {
        display: 'Replication',
        id: PAGES.LECTURE_10.REPLICATION,
        children: [
          { display: 'Redundancy and Data Availability' },
          { display: 'Replication in MongoDB' },
          { display: 'Automatic Failover' },
          { display: 'Deploying' }
        ]
      },
      {
        display: 'Assignments',
        id: PAGES.LECTURE_10.ASSIGNMENTS,
        children: [
          { display: 'Pagination Problem' },
          { display: 'Mongo Aggregations' },
          { display: 'Knight Tour' },
        ]
      }
    ],
  },
  {
    display: '11. Documentation, Testing and CD/CI',
    id: PAGES.LECTURE_11.ID,
    children: [
      {
        display: 'Documentation and Coding Styles',
        id: PAGES.LECTURE_11.DOCUMENTATION,
        children: [
          { display: 'Rest Documentation Tools' },
          { display: 'Java Documentation' },
          { display: 'Coding Conventions'},
        ]
      },
      {
        display: 'Testing - Play Test',
        id: PAGES.LECTURE_11.TESTING,
        children: [
          { display: 'Overview', id: 'testing_overview' },
          { display: 'Using JUnit' },
          { display: 'Unit testing controllers'},
        ]
      },
      {
        display: 'Continuous Development/Integration',
        id: PAGES.LECTURE_11.CD_CI,
        children: [
          { display: 'Introduction', id: "cd_ci_introduction" },
          { display: 'Continuous Integration' },
          { display: 'Continuous Delivery'},
          { display: 'Continuous Deployment'},
          { display: 'Workflow', id: 'cd_ci_workflow' },
          { display: 'Setup', id: 'cd_ci_setup' }
        ]
      },
      {
        display: 'Assignments',
        id: PAGES.LECTURE_11.ASSIGNMENTS,
        children: [
        ]
      }
    ],
  },
  {
    display: '12. Recap and Wrap Up!',
    id: PAGES.LECTURE_12.ID,
    children: [
    ],
  },
  {
    display: 'Materials',
    id: PAGES.SUPPORT.ID,
    children: [
      {
        display: 'Plan Program',
        id: PAGES.SUPPORT.PLAN_PROGRAM
      },
      {
        display: 'Glossary',
        id: PAGES.SUPPORT.GLOSSARY,
      },
      {
        display: 'Resources',
        id: PAGES.SUPPORT.RESOURCES,
      },
      // {
      //   display: 'Tips and Tricks',
      //   id: PAGES.SUPPORT.TIPS_AND_TRICKS,
      //   children: [
      //     {
      //       display: 'Debugging'
      //     },
      //     {
      //       display: 'Logging'
      //     },
      //     {
      //       display: 'React Dev Tools'
      //     }
      //   ]
      // },
    ]
  },
  {
    display: 'Playground',
    id: PAGES.PLAYGROUND,
  }
]

const format = (which) => {
  let children = which.children || []
  return {
    ...which,
    id: !which.id ? which.display.replace(/ /g, '').replace(/[^a-zA-Z]/g, '').toLowerCase() : which.id,
    children: children.map(format)
  }
}

export const findById = (id) => {
  return routes.reduce((flatten, next) => {
    let children = next.children || []
    return [...flatten, next, ...children]
  }, []).find(which => which.id === id)
}

/**
 * Created by LeutrimNeziri on 30/03/2019.
 */
export default routes.map(format)


