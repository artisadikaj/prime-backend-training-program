/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Highlighted } from "presentations/Label";
import Code from "presentations/Code";

const styles = ({ size, typography }) => ({
  root: {}
})

const helloActor = `import akka.actor.*;
import akka.japi.*;
import actors.HelloActorProtocol.*;

public class HelloActor extends AbstractActor {

  public static Props getProps() {
    return Props.create(HelloActor.class);
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder()
        .match(
            SayHello.class,
            hello -> {
              String reply = "Hello, " + hello.name;
              sender().tell(reply, self());
            })
        .build();
  }
}`

const helloActorProtocol = `public class HelloActorProtocol {

  public static class SayHello {
    public final String name;

    public SayHello(String name) {
      this.name = name;
    }
  }
}`

const askPattern = `public class LectureSevenController extends Controller {

	final ActorRef helloActor;

	@Inject
	public LectureSevenController(ActorSystem system) {
		helloActor = system.actorOf(HelloActor.getProps());
	}

	public CompletionStage<Result> sayHello(String name) {
		return FutureConverters.toJava(ask(helloActor, new HelloActorProtocol.SayHello(name), 1000))
				.thenApply(response -> ok((String) response));
	}
}`

const configuredActor = `public class ConfiguredActor extends AbstractActor {

	private Config configuration;

	@Inject
	public ConfiguredActor(Config configuration) {
		this.configuration = configuration;
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
			.match(ConfiguredActorProtocol.GetConfig.class, message -> {
				sender().tell(configuration.getString("environment"), self());
			})
			.build();
	}
}`

const actorModule = `public class ActorModule extends AbstractModule implements AkkaGuiceSupport {

    @Override
    protected void configure() {
        bindActor(ConfiguredActor.class, "configured-actor");
    }
}`

const callingConfiguredActor = `final ActorRef helloActor;
	private ActorRef configuredActor;

	@Inject
	public LectureSevenController(@Named("configured-actor") ActorRef configuredActor) {
		this.configuredActor = configuredActor;
	}

	public CompletionStage<Result> getConfig() {
		return FutureConverters.toJava(ask(configuredActor, new ConfiguredActorProtocol.GetConfig(), 1000))
			.thenApply(response -> ok((String) response));
	}
}`

const dependencyInjectedActor = `import akka.actor.AbstractActor;
import com.google.inject.assistedinject.Assisted;
import com.typesafe.config.Config;

import javax.inject.Inject;

public class ConfiguredChildActor extends AbstractActor {

  private final Config configuration;
  private final String key;

  @Inject
  public ConfiguredChildActor(Config configuration, @Assisted String key) {
    this.configuration = configuration;
    this.key = key;
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder()
        .match(ConfiguredChildActorProtocol.GetConfig.class, this::getConfig)
        .build();
  }

  private void getConfig(ConfiguredChildActorProtocol.GetConfig get) {
    sender().tell(configuration.getString(key), self());
  }
}`

const childActorProtocol = `public class ConfiguredChildActorProtocol {

	public static class GetConfig {}

	public interface Factory {
		public Actor create(String key);
	}
}`

const parentActor = `public class ParentActor extends AbstractActor implements InjectedActorSupport {

	private ConfiguredChildActorProtocol.Factory childFactory;

	@Inject
	public ParentActor(ConfiguredChildActorProtocol.Factory childFactory) {
		this.childFactory = childFactory;
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(ParentActorProtocol.GetChild.class, this::getChild).build();
	}

	private void getChild(ParentActorProtocol.GetChild msg) {
		String key = msg.getKey();
		ActorRef child = injectedChild(() -> childFactory.create(key), key);
		sender().tell(child, self());
	}
}`

const bindingActors = `public class ActorModule extends AbstractModule implements AkkaGuiceSupport {

    @Override
    protected void configure() {
        bindActor(ConfiguredActor.class, "configured-actor");
        bindActor(ParentActor.class, "parent-actor");
        bindActorFactory(ConfiguredChildActor.class, ConfiguredChildActorProtocol.Factory.class);
    }
}`

const confBinding = `akka.actor.default-dispatcher.fork-join-executor.parallelism-max = 64
akka.actor.debug.receive = on`

const logBackConfig = `<!-- https://www.playframework.com/documentation/latest/SettingsLogger -->
<configuration>
  ...
  <logger name="play.mvc.Action" level="ERROR" />
  <logger name="akka" level="ERROR" />
  <logger name="play" level="INFO" />
  <logger name="io.training.api" level="DEBUG" />
  ...
</configuration>`

const configPrefix = `my-akka.actor.default-dispatcher.fork-join-executor.parallelism-max = 64
my-akka.actor.debug.receive = on`

const codeAsync = `import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class Application extends Controller {
    public CompletionStage<Result> index() {
		return CompletableFuture.supplyAsync(this::longComputation)
				.thenApply((Integer i) -> ok("Got " + i));
	}

	private Integer longComputation() {
		return 2;
	}
}`

const sbtAkka = `
val akkaManagementVersion = "1.0.0"
val akkaVersion = "2.6.0"
val akkaHTTPVersion = "10.1.10"
...

libraryDependencies ++= Seq(
  ...
  // akka related stuff
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster-sharding" % akkaVersion,
  "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
  "com.typesafe.akka" %% "akka-distributed-data" % akkaVersion,
  "com.typesafe.akka" %% "akka-discovery" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
  "com.typesafe.akka" %% "akka-serialization-jackson" % akkaVersion,
  // akka cluster related stuff
  "com.lightbend.akka.discovery" %% "akka-discovery-kubernetes-api" % akkaManagementVersion,
  "com.lightbend.akka.management" %% "akka-management" % akkaManagementVersion,
  "com.lightbend.akka.management" %% "akka-management-cluster-http" % akkaManagementVersion,
  "com.lightbend.akka.management" %% "akka-management-cluster-bootstrap" % akkaManagementVersion,
  // akka htttp related stuff
  "com.typesafe.akka" %% "akka-http-core" % akkaHTTPVersion,
  "com.typesafe.akka" %% "akka-http2-support" % akkaHTTPVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHTTPVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHTTPVersion,
)`

const AkkaPlay = (props) => {
  const { classes, section } = props
  const actorSystem = section.children[0]
  const dependencyInjectionActors = section.children[1]
  const configuration = section.children[2]
  const codeBlockAsync = section.children[3]
  const coordinatedShutDown = section.children[4]
  const updatingAkkaVersion = section.children[5]

  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 7: {section.display}
        <Typography>
          Resources: <ol>
          <li><SimpleLink href="https://akka.io/">https://akka.io/</SimpleLink></li>
          <li><SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaAkka">https://www.playframework.com/documentation/2.8.x/JavaAkka</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/guide/index.html">https://doc.akka.io/docs/akka/2.6.0/typed/guide/index.html</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/actors.html">https://doc.akka.io/docs/akka/2.6.0/actors.html</SimpleLink></li>
        </ol>

        </Typography>
        <Divider />
      </Typography>
      <Typography>
        <SimpleLink href="https://akka.io/">Akka</SimpleLink> is a toolkit for building highly concurrent, distributed, and resilient message-driven applications for Java and Scala.
      </Typography>
      <Typography>
        <SimpleLink href="https://akka.io/">Akka</SimpleLink> uses the Actor Model to raise the abstraction level and provide a better platform to build correct concurrent and scalable applications. For fault-tolerance it adopts the ‘Let it crash’ model, which has been used with great success in the telecoms industry to build applications that self-heal - systems that never stop. Actors also provide the abstraction for transparent distribution and the basis for truly scalable and fault-tolerant applications.
      </Typography>
      <Typography id={actorSystem.id} variant={'title'}>
        {actorSystem.display}
      </Typography>
      <Typography>
        Akka can work with several containers called actor systems. An actor system manages the resources it is configured to use in order to run the actors which it contains.
      </Typography>
      <Typography>
        A Play application defines a special actor system to be used by the application. This actor system follows the application life-cycle and restarts automatically when the application restarts.
      </Typography>
      <Typography variant="section">
        Writing actors
      </Typography>
      <Typography>
        To start using Akka, you need to write an actor. Below is a simple actor that simply says hello to whoever asks it to.
        <Code>
          {helloActor}
        </Code>
        Notice here that the <Highlighted>HelloActor</Highlighted> defines a static method called <Highlighted>getProps</Highlighted>, this method returns a Props object that describes how to create the actor. This is a good Akka convention, to separate the instantiation logic from the code that creates the actor.
      </Typography>
      <Typography>
        Another best practice shown here is that the messages that <Highlighted>HelloActor</Highlighted> sends and receives are defined as static inner classes of another class called <Highlighted>HelloActorProtocol</Highlighted>:
        <Code>
          {helloActorProtocol}
        </Code>
      </Typography>
      <Typography variant="section">
        Creating and using actors
      </Typography>
      <Typography>
        To create and/or use an actor, you need an <Highlighted>ActorSystem</Highlighted>. This can be obtained by declaring a dependency on an ActorSystem, then you can use the <Highlighted>actorOf</Highlighted> method to create a new actor.
      </Typography>
      <Typography>
        The most basic thing that you can do with an actor is send it a message. When you send a message to an actor, there is no response, it’s fire and forget. This is also known as the tell pattern.
      </Typography>
      <Typography>
        In a web application however, the tell pattern is often not useful, since HTTP is a protocol that has requests and responses. In this case, it is much more likely that you will want to use the ask pattern. The ask pattern returns a Scala Future, which you can convert to a Java <Highlighted>CompletionStage</Highlighted> using <Highlighted>scala.compat.java8.FutureConverts.toJava</Highlighted>, and then map to your own result type.
      </Typography>
      <Typography>
        Below is an example of using our <Highlighted>HelloActor</Highlighted> with the ask pattern:
        <Code>
          {askPattern}
        </Code>
        A few things to notice:
        <ol>
          <li>The ask pattern needs to be imported, it’s often most convenient to static import the <Highlighted>ask</Highlighted> method.</li>
          <li>The returned future is converted to a CompletionStage. The resulting promise is a <Highlighted>{`CompletionStage<Object>`}</Highlighted>, so when you access its value, you need to cast it to the type you are expecting back from the actor.</li>
          <li>The ask pattern requires a timeout, we have supplied 1000 milliseconds. If the actor takes longer than that to respond, the returned promise will be completed with a timeout error.</li>
          <li>Since we’re creating the actor in the constructor, we need to scope our controller as <Highlighted>Singleton</Highlighted>, so that a new actor isn’t created every time this controller is used.</li>
        </ol>
      </Typography>
      <Typography id={dependencyInjectionActors.id} variant={'title'}>
        {dependencyInjectionActors.display}
      </Typography>
      <Typography>
        If you prefer, you can have Guice instantiate your actors and bind actor refs to them for your controllers and components to depend on.
      </Typography>
      <Typography>
        For example, if you wanted to have an actor that depended on the Play configuration, you might do this:
        <Code>
          {configuredActor}
        </Code>
        Play provides some helpers to help providing actor bindings. These allow the actor itself to be dependency injected, and allows the actor ref for the actor to be injected into other components. To bind an actor using these helpers, create a module as described in the dependency injection documentation, then mix in the <Highlighted>AkkaGuiceSupport</Highlighted> interface and use the bindActor method to bind the actor:
        <Code>
          {actorModule}
        </Code>
        This actor will both be named <Highlighted>configured-actor</Highlighted>, and will also be qualified with the <Highlighted>configured-actor</Highlighted> name for injection. You can now depend on the actor in your controllers and other components:
        <Code>
          {callingConfiguredActor}
        </Code>
      </Typography>

      <Typography variant="section">
        Dependency injecting child actors
      </Typography>
      <Typography>
        The above is good for injecting root actors, but many of the actors you create will be child actors that are not bound to the lifecycle of the Play app, and may have additional state passed to them.
      </Typography>
      <Typography>
        In order to assist in dependency injecting child actors, Play utilises Guice’s <SimpleLink href="https://github.com/google/guice/wiki/AssistedInject">AssistedInject</SimpleLink> support.
      </Typography>
      <Typography>
        Let’s say you have the following actor, which depends on configuration to be injected, plus a key:
        <Code>
          {dependencyInjectedActor}
        </Code>
        In this case we have used constructor injection - Guice’s assisted inject support is only compatible with constructor injection. Since the key parameter is going to be provided on creation, not by the container, we have annotated it with <Highlighted>@Assisted</Highlighted>.
      </Typography>
      <Typography>
        Now in the protocol for the child, we define a <Highlighted>Factory</Highlighted> interface that takes the key and returns the <Highlighted>Actor</Highlighted>:
        <Code>
          {childActorProtocol}
        </Code>
        We won’t implement this, Guice will do that for us, providing an implementation that not only passes our <Highlighted>key</Highlighted> parameter, but also locates the <Highlighted>Configuration</Highlighted> dependency and injects that. Since the trait just returns an Actor, when testing this actor we can inject a factor that returns any actor, for example this allows us to inject a mocked child actor, instead of the actual one.
      </Typography>
      <Typography>
        Now, the actor that depends on this can extend <Highlighted>InjectedActorSupport</Highlighted>, and it can depend on the factory we created:
        <Code>
          {parentActor}
        </Code>
        It uses the injectedChild to create and get a reference to the child actor, passing in the key. The second parameter (key in this example) will be used as the child actor’s name.
      </Typography>
      <Typography>
        Finally, we need to bind our actors. In our module, we use the bindActorFactory method to bind the parent actor, and also bind the child factory to the child implementation:
        <Code>
          {bindingActors}
        </Code>
        This will get Guice to automatically bind an instance of <Highlighted>ConfiguredChildActorProtocol.Factory</Highlighted>, which will provide an instance of <Highlighted>Configuration</Highlighted> to <Highlighted>ConfiguredChildActor</Highlighted> when it’s instantiated.
      </Typography>
      <Typography id={configuration.id} variant={'title'}>
        {configuration.display}
      </Typography>
      <Typography>
        The default actor system configuration is read from the Play application configuration file. For example, to configure the default dispatcher of the application actor system, add these lines to the <Highlighted>conf/application.conf</Highlighted> file:
        <Code>
          {confBinding}
        </Code>
        For Akka logging configuration, see logging config, that is the <Highlighted>logback.xml</Highlighted>:
        <Code>
          {logBackConfig}
        </Code>
      </Typography>
      <Typography variant="section">
        Changing configuration prefix
      </Typography>
      <Typography>
        In case you want to use the <Highlighted>akka.*</Highlighted> settings for another Akka actor system, you can tell Play to load its Akka settings from another location.
        <Code>
          play.akka.config = "my-akka"
        </Code>
        Now settings will be read from the <Highlighted>my-akka</Highlighted> prefix instead of the <Highlighted>akka</Highlighted> prefix.
        <Code>
          {configPrefix}
        </Code>
      </Typography>
      <Typography variant="section">
        Built-in actor system name
      </Typography>
      <Typography>
        By default the name of the Play actor system is application. You can change this via an entry in the <Highlighted>conf/application.conf</Highlighted>:
        <Code>
          play.akka.actor-system = "custom-name"
        </Code>
      </Typography>
      <Typography id={codeBlockAsync.id} variant={'title'}>
        {codeBlockAsync.display}
      </Typography>
      <Typography>
        A common use case within Akka is to have some computation performed concurrently without needing the extra utility of an Actor. If you find yourself creating a pool of Actors for the sole reason of performing a calculation in parallel, there is an easier (and faster) way:
        <Code>
          {codeAsync}
        </Code>
      </Typography>
      <Typography id={coordinatedShutDown.id} variant={'title'}>
        {coordinatedShutDown.display}
      </Typography>
      <Typography>
        Play handles the shutdown of the Application and the Server using Akka’s <SimpleLink href="https://doc.akka.io/docs/akka/2.6/actors.html?language=java#coordinated-shutdown">Coordinated Shutdown</SimpleLink>. Find more information in the Coordinated Shutdown common section.
      </Typography>
      <Typography>
        NOTE: Play only handles the shutdown of its internal ActorSystem. If you are using extra actor systems, make sure they are all terminated and feel free to migrate your termination code to Coordinated Shutdown.
      </Typography>
      <Typography id={updatingAkkaVersion.id} variant={'title'}>
        {updatingAkkaVersion.display}
      </Typography>
      <Typography>
        If you want to use a newer version of Akka, one that is not used by Play yet, you can add the following to your <Highlighted>build.sbt</Highlighted> file:
        <Code>
          {sbtAkka}
        </Code>
      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(AkkaPlay)
