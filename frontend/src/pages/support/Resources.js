/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
const styles = ({ typography }) => ({
  root: {},
})

class Resources extends React.Component {
  render() {
    const { classes } = this.props
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Resources
          <Divider />
        </Typography>
        <Typography variant='p'>
          Setup and Install
          <ul>
            <li><label>Install Node: <SimpleLink href="https://nodejs.org/en/download/">https://nodejs.org/en/download/</SimpleLink></label></li>
            <li><label>Install Source Tree: <SimpleLink href="https://www.sourcetreeapp.com/">https://www.sourcetreeapp.com/</SimpleLink></label></li>
            <li><label>Install Visual Studio Code: <SimpleLink href="https://code.visualstudio.com/">https://code.visualstudio.com/</SimpleLink></label></li>
            <li><label>Install IntelliJ: <SimpleLink href=" https://www.jetbrains.com/idea/">https://www.jetbrains.com/idea/</SimpleLink></label></li>
            <li><label>Slack Workspace: <SimpleLink href="https://join.slack.com/t/backendtraining/shared_invite/zt-grop9uw0-3UzNrjY~QRDIw6OLi0GRnw">Accept Invite</SimpleLink></label></li>
            <li><label>Slack Workspace URL: <SimpleLink href="https://backendtraining.slack.com/">https://backendtraining.slack.com/</SimpleLink></label></li>
            <li><label>Slack Install: <SimpleLink href="https://slack.com/download">https://slack.com/download</SimpleLink></label></li>
            <li><label>Postman: <SimpleLink href="https://www.postman.com/">https://www.postman.com/</SimpleLink></label></li>
            <li><label>GitLab: <SimpleLink href="https://gitlab.com">https://gitlab.com</SimpleLink></label></li>
            <li><label>GIT: <SimpleLink href="https://git-scm.com/">https://git-scm.com/</SimpleLink></label></li>
            <li><label>Working with GIT: <SimpleLink href="https://product.hubspot.com/blog/git-and-github-tutorial-for-beginners">https://product.hubspot.com/blog/git-and-github-tutorial-for-beginners</SimpleLink></label></li>
            <li><label>MongoDB: <SimpleLink href="https://www.mongodb.com/">https://www.mongodb.com/</SimpleLink></label></li>
            <li><label>Robomongo: <SimpleLink href="https://robomongo.org/download">https://robomongo.org/download</SimpleLink></label></li>
            <li><label>JSON: <SimpleLink href="https://www.json.org/">https://www.json.org/</SimpleLink></label></li>
            <li><label>W3Schools JS: <SimpleLink href="https://www.w3schools.com/js/">https://www.w3schools.com/js/</SimpleLink></label></li>
            <li><label>Play Framework: <SimpleLink href="https://www.playframework.com/">https://www.playframework.com/</SimpleLink></label></li>
            <li><label>Scala Build Tool (SBT): <SimpleLink href="https://www.scala-sbt.org/">https://www.scala-sbt.org/</SimpleLink></label></li>
            <li><label>Java Releases: <SimpleLink href="http://www.oracle.com/technetwork/java/javase/downloads/">http://www.oracle.com/technetwork/java/javase/downloads/</SimpleLink></label></li>
            <li><label>Play Java Main Concepts: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaHome">https://www.playframework.com/documentation/2.8.x/JavaHome</SimpleLink></label></li>
            <li><label>Akka Documentation: <SimpleLink href="https://doc.akka.io/docs/akka/2.6/index.html">https://doc.akka.io/docs/akka/2.6/index.html</SimpleLink></label></li>
          </ul>
          Udemy Classes:
          <ul>
            <li><label>Java Master Class: <SimpleLink href="https://www.udemy.com/course/java-the-complete-java-developer-course/">https://www.udemy.com/course/java-the-complete-java-developer-course/</SimpleLink></label></li>
            <li><label>Introduction to Scala: <SimpleLink href="https://www.udemy.com/course/scala-in-practice/">https://www.udemy.com/course/scala-in-practice/</SimpleLink></label></li>
            <li><label>Play Framework: <SimpleLink href="https://www.udemy.com/course/play-framework-for-web-application-development/">https://www.udemy.com/course/play-framework-for-web-application-development/</SimpleLink></label></li>
            <li><label>MongoDB: <SimpleLink href="https://www.udemy.com/course/mongodb-the-complete-developers-guide/">https://www.udemy.com/course/mongodb-the-complete-developers-guide/</SimpleLink></label></li>
          </ul>
          Books:
          <ul>
            <li><label>Head First Java</label></li>
            <li><label>Play for Java</label></li>
            <li><label>Play Framework Essentials</label></li>
            <li><label>Introducing Play Framework</label></li>
            <li><label>MongoDB Recipes</label></li>
          </ul>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(Resources)
