/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import PageLink from "presentations/rows/nav/PageLink";
import Code from "presentations/Code";
import { Bold, Highlighted } from "presentations/Label";
import MemberStates from 'assets/images/lecture8/member-states.png'
import MemberStatesWeaklyUp from 'assets/images/lecture8/member-states-weakly-up.png'

const styles = ({ size, typography }) => ({
  root: {
  },
  img: {
    boxShadow: 'none'
  }
})
const akkaConfig = `akka {
  # Options: OFF, ERROR, WARNING, INFO, DEBUG
  loglevel = INFO
  actor {
    allow-java-serialization = on
    serializers {
      # different type of serializers and their implementers
      jackson-json = "akka.serialization.jackson.JacksonJsonSerializer"
      java = "akka.serialization.JavaSerializer"
      proto = "akka.remote.serialization.ProtobufSerializer"
    }
    serialization-bindings {
      # bind an interface to a serializer declared above
      "io.training.api.actors.ActorMessage" = jackson-json
    }
    # its a cluster provider for akka aktor system
    provider = "cluster"
    debug {
      # enable DEBUG logging of all AutoReceiveMessages (Kill, PoisonPill etc.)
      autoreceive = on
      # enable DEBUG logging of actor lifecycle changes
      lifecycle = on
    }
  }
  # name of our actor system
  actor-system = "backend-training"
  # discovery configration -> where will the members of a cluster discover one another
  discovery {
    config.services = {
      backend-training = {
        endpoints = [
          {
            host = "127.0.0.1"
            port = 8558
          }
        ]
      }
    }
  }
  # akka communictaion on remote
  remote {
    artery {
      enabled = on
      transport = tcp
      canonical.port = 2551
      canonical.hostname = "127.0.0.1"
    }
  }
  # cluster level configuration
  cluster {
    # the minimum number of members for which the cluster can work
    min-nr-of-members = 1
    shutdown-after-unsuccessful-join-seed-nodes = 30s
  }
  log-dead-letters = on
  coordinated-shutdown.exit-jvm = off
  management {
    http {
      route-providers-read-only = false
      hostname = "127.0.0.1"
    }
    cluster.bootstrap {
      contact-point-discovery {
        service-name = "backend-training"
        discovery-method = config,
        required-contact-point-nr = 1
      }
    }
  }
}`

const register = ` // Akka Management hosts the HTTP routes used by bootstrap
AkkaManagement.get(system).start();

// Starting the bootstrap process needs to be done explicitly
ClusterBootstrap.get(system).start();

final Cluster cluster = Cluster.get(system);
cluster.registerOnMemberUp(() -> {
    Logger.of(this.getClass()).debug("MEMBER IS UP");
});`

const logs = `[INFO] [...] - Node [akka://application@127.0.0.1:2551] is JOINING itself (with roles [dc-default]) and forming new cluster
[INFO] [...] - is the new leader among reachable nodes (more leaders may exist)
[INFO] [...] - Leader is moving node [akka://application@127.0.0.1:2551] to [Up]
[debug] i.t.a.m.ApplicationStartProvider - MEMBER IS UP`

const AkkaCluster = (props) => {
  const { classes, section } = props
  const gettingStarted = section.children[0]
  const specification = section.children[1]
  const membership = section.children[2]
  const serialization = section.children[3]

  const ClusterMembershipService = <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/cluster-membership.html#cluster-membership-service">Cluster Membership Service</SimpleLink>
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 8: {section.display}
        <Typography>
          Build powerful reactive, concurrent, and distributed applications more easily
        </Typography>
        <Divider />
      </Typography>
      <Typography>
        In a typical web application sooner or later you would have to encounter one of the following challenges, especially after having to scale your services horizontally:
        <ol>
          <li>Real time chat between multiple users using a service which is deployed on multiple physical locations</li>
          <li>Distributed computing. Divide and Conquer a task by utilizing multiple machines at the same time, in parallel to process a request</li>
          <li>Distributed Background Jobs</li>
          <li>Shared Data between the whole members of a cluster</li>
          <li>etc...</li>
        </ol>
        the need to start programming in a distributed manner to tackle these challenges easier becomes more prominent and urgent.
      </Typography>
      <Typography fontStyle="italic">
        Two types of scaling and their definition:
        <ol>
          <li>Vertical scaling: The increase of compute/store capacities by using a more powerful machine (server) or virtual environment</li>
          <li>Horizontal scaling: The increase of compute/store capacities by replicating the same solution/service and deploy it on different machines or virtual environment</li>
        </ol>
      </Typography>
      <Typography>
        For distributed programming Akka contains the set of tools that allows the user to scale a service (web application) horizontally, build complex messaging systems, process requests in parallel in a high efficiency and more with ease.
      </Typography>
      <Typography>
        In concrete words, the challenges that the Cluster module solves include the following:
        <ol>
          <li>How to maintain a set of actor systems (a cluster) that can communicate with each other and consider each other as part of the cluster.</li>
          <li>How to introduce a new system safely to the set of already existing members.</li>
          <li>How to reliably detect systems that are temporarily unreachable.</li>
          <li>How to remove failed hosts/systems (or scale down the system) so that all remaining members agree on the remaining subset of the cluster.</li>
          <li>How to distribute computations among the current set of members.</li>
          <li>How to designate members of the cluster to a certain role, in other words, to provide certain services and not others.</li>
        </ol>
      </Typography>
      <Typography id={gettingStarted.id} variant={'title'}>
        {gettingStarted.display}
      </Typography>
      <Typography>
        In order to setup your first cluster locally, we need to do the following:
        <ol>
          <li>Choose and import the Akka modules: <PageLink to="/section/updatingakkaversion/">Akka Version</PageLink></li>
          <li>Prepare the application configuration <Code>{akkaConfig}</Code></li>
          <li>Tell the play application to join the cluster once the Play app starts: <Code>{register}</Code>
            The source code can be found at <Highlighted>ApplicationStartProvider.class</Highlighted></li>
        </ol>
        Which than if you run the application you will get something like this (if everything went well):
        <Code>
          {logs}
        </Code>
      </Typography>
      <Typography>
        The full guide on getting started can be found here: <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/cluster.html">https://doc.akka.io/docs/akka/2.6.0/typed/cluster.html</SimpleLink>
      </Typography>
      <Typography id={specification.id} variant={'title'}>
        {specification.display}
      </Typography>
      <Typography>
        Akka Cluster provides a fault-tolerant decentralized peer-to-peer based {ClusterMembershipService} with no single point of failure or single point of bottleneck. It does this using gossip protocols and an automatic failure detector.
      </Typography>
      <Typography>
        Akka Cluster allows for building distributed applications, where one application or service spans multiple nodes (in practice multiple <Highlighted>ActorSystem</Highlighted>s).
      </Typography>
      <Typography>
        Terms and definitions:
        <ol>
          <li><Bold>node</Bold>: A logical member of a cluster. There could be multiple nodes on a physical machine. Defined by a hostname:port:uid tuple.</li>
          <li><Bold>cluster</Bold>: A set of nodes joined together through the {ClusterMembershipService}.</li>
          <li><Bold>leader</Bold>: A single node in the cluster that acts as the leader. Managing cluster convergence and membership state transitions.</li>
        </ol>
      </Typography>
      <Typography variant="section">
        Gossip
      </Typography>
      <Typography>
        The cluster membership used in Akka is based on Amazon’s Dynamo system and particularly the approach taken in Basho’s’ Riak distributed database. Cluster membership is communicated using a <SimpleLink href="https://en.wikipedia.org/wiki/Gossip_protocol">Gossip Protocol</SimpleLink>, where the current state of the cluster is gossiped randomly through the cluster, with preference to members that have not seen the latest version.
      </Typography>
      <Typography variant="section">
        Gossip Convergence
      </Typography>
      <Typography>
        Information about the cluster converges locally at a node at certain points in time. This is when a node can prove that the cluster state he is observing has been observed by all other nodes in the cluster. Convergence is implemented by passing a set of nodes that have seen current state version during gossip. This information is referred to as the seen set in the gossip overview. When all nodes are included in the seen set there is convergence.
      </Typography>
      <Typography>
        Gossip convergence cannot occur while any nodes are <Highlighted>unreachable</Highlighted>. The nodes need to become <Highlighted>reachable</Highlighted> again, or moved to the <Highlighted>down</Highlighted> and removed states (see the <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/cluster-membership.html#membership-lifecycle">Cluster Membership Lifecycle</SimpleLink> section). This only blocks the leader from performing its cluster membership management and does not influence the application running on top of the cluster. For example this means that during a network partition it is not possible to add more nodes to the cluster. The nodes can join, but they will not be moved to the up state until the partition has healed or the unreachable nodes have been downed.
      </Typography>
      <Typography variant="section">
        Vector clocks
      </Typography>
      <Typography>
        <SimpleLink href="https://en.wikipedia.org/wiki/Vector_clock">Vector clocks</SimpleLink> are a type of data structure and algorithm for generating a partial ordering of events in a distributed system and detecting causality violations.

        We use vector clocks to reconcile and merge differences in cluster state during gossiping. A vector clock is a set of (node, counter) pairs. Each update to the cluster state has an accompanying update to the vector clock.
      </Typography>
      <Typography variant="section">
        Failure Detector
      </Typography>
      <Typography>
        The failure detector in Akka Cluster is responsible for trying to detect if a node is unreachable from the rest of the cluster. For this we are using the <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/failure-detector.html">Phi Accrual Failure Detector</SimpleLink> implementation. To be able to survive sudden abnormalities, such as garbage collection pauses and transient network failures the failure detector is easily configurable for tuning to your environments and needs.
      </Typography>
      <Typography>
        The full guide on getting started can be found here: <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/cluster-concepts.html">https://doc.akka.io/docs/akka/2.6.0/typed/cluster-concepts.html</SimpleLink>
      </Typography>
      <Typography id={membership.id} variant={'title'}>
        {membership.display}
      </Typography>
      <Typography>
        The core of Akka Cluster is the cluster membership, to keep track of what nodes are part of the cluster and their health. Cluster membership is communicated using gossip and failure detection.
      </Typography>
      <Typography>
        There are several <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/cluster.html#higher-level-cluster-tools">Higher level Cluster</SimpleLink> tools that are built on top of the cluster membership service.
      </Typography>
      <Typography>
        A cluster is made up of a set of member nodes. The identifier for each node is a hostname:port:uid tuple. An Akka application can be distributed over a cluster with each node hosting some part of the application. Cluster membership and the actors running on that node of the application are decoupled. A node could be a member of a cluster without hosting any actors. Joining a cluster is initiated by issuing a Join command to one of the nodes in the cluster to join.
      </Typography>
      <Typography>
        The node identifier internally also contains a UID that uniquely identifies this actor system instance at that hostname:port. Akka uses the UID to be able to reliably trigger remote death watch. This means that the same actor system can never join a cluster again once it’s been removed from that cluster. To re-join an actor system with the same hostname:port to a cluster you have to stop the actor system and start a new one with the same hostname:port which will then receive a different UID.
      </Typography>

      <Typography variant="section">
        Member States
      </Typography>
      <Typography>
        The cluster membership state is a specialized <SimpleLink href="https://hal.inria.fr/file/index/docid/555588/filename/techreport.pdf">CRDT</SimpleLink>, which means that it has a monotonic merge function. When concurrent changes occur on different nodes the updates can always be merged and converge to the same end result.
        <ol>
          <li><Bold>joining</Bold> - transient state when joining a cluster</li>
          <li><Bold>weakly up</Bold> - transient state while network split (only if akka.cluster.allow-weakly-up-members=on)</li>
          <li><Bold>up</Bold> - normal operating state</li>
          <li><Bold>leaving/ exiting</Bold>  - states during graceful removal</li>
          <li><Bold>down</Bold> - marked as down (no longer part of cluster decisions)</li>
          <li><Bold>removed</Bold> - tombstone state (no longer a member)</li>
        </ol>
      </Typography>
      <Typography variant="section">
        Member Events
      </Typography>
      <Typography>
        The events to track the life-cycle of members are:
        <ol>
          <li><Highlighted>ClusterEvent.MemberJoined</Highlighted> - A new member has joined the cluster and its status has been changed to Joining</li>
          <li><Highlighted>ClusterEvent.MemberUp</Highlighted> - A new member has joined the cluster and its status has been changed to Up</li>
          <li><Highlighted>ClusterEvent.MemberExited</Highlighted> - A member is leaving the cluster and its status has been changed to Exiting Note that the node might already have been shutdown when this event is published on another node.</li>
          <li><Highlighted>ClusterEvent.MemberRemoved</Highlighted> - Member completely removed from the cluster.</li>
          <li><Highlighted>ClusterEvent.UnreachableMember</Highlighted> - A member is considered as unreachable, detected by the failure detector of at least one other node.</li>
          <li><Highlighted>ClusterEvent.ReachableMember</Highlighted> - A member is considered as reachable again, after having been unreachable. All nodes that previously detected it as unreachable has detected it as reachable again.</li>
        </ol>
      </Typography>
      <Typography variant="section">
        Membership Lifecycle
      </Typography>
      <Typography>
        A node begins in the joining state. Once all nodes have seen that the new node is joining (through gossip convergence) the leader will set the member state to up.
      </Typography>
      <Typography>
        If a node is leaving the cluster in a safe, expected manner then it switches to the leaving state. Once the leader sees the convergence on the node in the leaving state, the leader will then move it to exiting. Once all nodes have seen the exiting state (convergence) the leader will remove the node from the cluster, marking it as removed.
      </Typography>
      <Typography>
        If a node is unreachable then gossip convergence is not possible and therefore any leader actions are also not possible (for instance, allowing a node to become a part of the cluster). To be able to move forward the state of the unreachable nodes must be changed. It must become reachable again or marked as down. If the node is to join the cluster again the actor system must be restarted and go through the joining process again. If new incarnation of the unreachable node tries to rejoin the cluster old incarnation will be marked as down and new incarnation can rejoin the cluster without manual intervention.
      </Typography>
      <Typography variant="section">
        WeaklyUp Members
      </Typography>
      <Typography>
        If a node is unreachable then gossip convergence is not possible and therefore any leader actions are also not possible. However, we still might want new nodes to join the cluster in this scenario.
      </Typography>
      <Typography>
        Joining members will be promoted to WeaklyUp and become part of the cluster if convergence can’t be reached. Once gossip convergence is reached, the leader will move WeaklyUp members to Up.
      </Typography>
      <Typography>
        This feature is enabled by default, but it can be disabled with configuration option:
        <Code>
          akka.cluster.allow-weakly-up-members = off
        </Code>
        You can subscribe to the WeaklyUp membership event to make use of the members that are in this state, but you should be aware of that members on the other side of a network partition have no knowledge about the existence of the new members. You should for example not count WeaklyUp members in quorum decisions.
      </Typography>
      <Typography variant="section">
        State Diagrams
      </Typography>
      <Typography>
        State Diagram for the Member States (akka.cluster.allow-weakly-up-members=off)
      </Typography>
      <img src={MemberStates} />
      <Typography>
        State Diagram for the Member States (akka.cluster.allow-weakly-up-members=on)
      </Typography>
      <img src={MemberStatesWeaklyUp} />
      <Typography>
        User Actions
        <ul>
          <li><Bold>join</Bold> - join a single node to a cluster - can be explicit or automatic on startup if a node to join have been specified in the configuration</li>
          <li><Bold>leave</Bold> - tell a node to leave the cluster gracefully</li>
          <li><Bold>down</Bold> - mark a node as down</li>
        </ul>
        Leader Actions. The leader has the following duties:
        <ul>
          <li>
            shifting members in and out of the cluster
            <ul>
              <li>joining -> up</li>
              <li>weakly up -> up (no convergence is required for this leader action to be performed)</li>
              <li>exiting -> removed</li>
            </ul>
          </li>
        </ul>
        Failure Detection and Unreachability
        <ul>
          <li><Bold>fd*</Bold> - the failure detector of one of the monitoring nodes has triggered causing the monitored node to be marked as unreachable</li>
          <li><Bold>unreachable*</Bold> - unreachable is not a real member states but more of a flag in addition to the state signaling that the cluster is unable to talk to this node, after being unreachable the failure detector may detect it as reachable again and thereby remove the flag</li>
        </ul>
      </Typography>

      <Typography id={serialization.id} variant={'title'}>
        {serialization.display}
      </Typography>
      <Typography>
        The messages that Akka actors send to each other are JVM objects . Message passing between actors that live on the same JVM is straightforward. It is done via reference passing. However, messages that have to escape the JVM to reach an actor running on a different host have to undergo some form of serialization (i.e. the objects have to be converted to and from byte arrays).
      </Typography>
      <Typography>
        The serialization mechanism in Akka allows you to write custom serializers and to define which serializer to use for what.
      </Typography>
      <Typography>
        <SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/serialization-jackson.html">Serialization with Jackson</SimpleLink> is a good choice in many cases and our recommendation if you don’t have other preference.
      </Typography>
      <Typography>
        <SimpleLink href="https://developers.google.com/protocol-buffers/">Google Protocol Buffers</SimpleLink> is good if you want more control over the schema evolution of your messages, but it requires more work to develop and maintain the mapping between serialized representation and domain representation.
      </Typography>
      <Typography>
        If you want to learn more about the topics covered here, follow:
        <ol>
          <li><SimpleLink href="https://akka.io/">https://akka.io/</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/index-cluster.html">https://doc.akka.io/docs/akka/2.6.0/typed/index-cluster.html</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/cluster.html">https://doc.akka.io/docs/akka/2.6.0/typed/cluster.html</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/cluster-concepts.html">https://doc.akka.io/docs/akka/2.6.0/typed/cluster-concepts.html</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/typed/cluster-membership.html">https://doc.akka.io/docs/akka/2.6.0/typed/cluster-membership.html</SimpleLink></li>
        </ol>
        If you want to learn more about serialization and serialization using jackson refer to:
        <ol>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/serialization.html">Serialization</SimpleLink></li>
          <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6.0/serialization-jackson.html">Serialization using Jackson</SimpleLink></li>
        </ol>
      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(AkkaCluster)
