/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Bold, Highlighted } from "presentations/Label";
import Code from "presentations/Code";

const styles = ({ typography }) => ({
  root: {},
})

const jsonBody = `public Result index(Http.Request request) {
  JsonNode json = request.body().asJson();
  return ok("Got name: " + json.get("name").asText());
}`


const bufferMemorySetting = `play.http.parser.maxMemoryBuffer = 256K`

const bodyParserAbstractMethod = `public abstract Accumulator<ByteString, F.Either<Result, A>> apply(RequestHeader request);`

const bodyParserMethod = `public static class UserBodyParser implements BodyParser<User> {

private BodyParser.Json jsonParser;
private Executor executor;

@Inject
public UserBodyParser(BodyParser.Json jsonParser, Executor executor) {
  this.jsonParser = jsonParser;
  this.executor = executor;
}`

const implementation = `public Accumulator<ByteString, F.Either<Result, User>> apply(RequestHeader request) {
  Accumulator<ByteString, F.Either<Result, JsonNode>> jsonAccumulator =
      jsonParser.apply(request);
  return jsonAccumulator.map(
      resultOrJson -> {
        if (resultOrJson.left.isPresent()) {
          return F.Either.Left(resultOrJson.left.get());
        } else {
          JsonNode json = resultOrJson.right.get();
          try {
            User user = play.libs.Json.fromJson(json, User.class);
            return F.Either.Right(user);
          } catch (Exception e) {
            return F.Either.Left(
                Results.badRequest("Unable to read User from json: " + e.getMessage()));
          }
        }
      },
      executor);
}`

const customBodyParserUsage = `@BodyParser.Of(UserBodyParser.class)
public Result save(Http.Request request) {
  RequestBody body = request.body();
  User user = body.as(User.class);

  return ok("Got: " + user.name);
}`

const customContentLengthParser = `// Accept only 10KB of data.
public static class Text10Kb extends BodyParser.Text {
  @Inject
  public Text10Kb(HttpErrorHandler errorHandler) {
    super(10 * 1024, errorHandler);
  }
}

@BodyParser.Of(Text10Kb.class)
public Result index(Http.Request request) {
  return ok("Got body: " + request.body().asText());
}`

const redirectingRequest = `public static class ForwardingBodyParser implements BodyParser<WSResponse> {
  private WSClient ws;
  private Executor executor;

  @Inject
  public ForwardingBodyParser(WSClient ws, Executor executor) {
    this.ws = ws;
    this.executor = executor;
  }

  String url = "http://example.com";

  public Accumulator<ByteString, F.Either<Result, WSResponse>> apply(RequestHeader request) {
    Accumulator<ByteString, Source<ByteString, ?>> forwarder = Accumulator.source();

    return forwarder.mapFuture(
        source -> {
          // TODO: when streaming upload has been implemented, pass the source as the body
          return ws.url(url)
              .setMethod("POST")
              // .setBody(source)
              .execute()
              .thenApply(F.Either::Right);
        },
        executor);
  }
}`

class ManipulatingResponses extends React.Component {
  render() {
    const { classes, section } = this.props
    const whatIsABodyParser = section.children[0]
    const buildInBodyParsers = section.children[1]
    const customBodyParsers = section.children[2]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Section 4: {section.display}
          <Typography>
            Resources: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaBodyParsers">https://www.playframework.com/documentation/2.8.x/JavaBodyParsers</SimpleLink>
          </Typography>
          <Divider />
        </Typography>
        <Typography id={whatIsABodyParser.id} variant={'title'}>
          {whatIsABodyParser.display}
        </Typography>
        <Typography>
          An HTTP request is a header followed by a body. The header is typically small - it can be safely buffered in memory, hence in Play it is modelled using the <Highlighted>RequestHeader</Highlighted> class. The body however can be potentially very long, and so is not buffered in memory, but rather is modelled as a stream. However, many request body payloads are small and can be modelled in memory, and so to map the body stream to an object in memory, Play provides a BodyParser abstraction.
        </Typography>
        <Typography>
          Since Play is an asynchronous framework, the traditional <Highlighted>InputStream</Highlighted> can’t be used to read the request body - input streams are blocking, when you invoke <Highlighted>read</Highlighted>, the thread invoking it must wait for data to be available. Instead, Play uses an asynchronous streaming library called Akka Streams. <SimpleLink href="https://doc.akka.io/docs/akka/2.6/stream/index.html?language=java">Akka Streams</SimpleLink> is an implementation of <SimpleLink href="http://www.reactive-streams.org/">Reactive Streams</SimpleLink>, a SPI that allows many asynchronous streaming APIs to seamlessly work together, so though traditional InputStream based technologies are not suitable for use with Play, Akka Streams and the entire ecosystem of asynchronous libraries around Reactive Streams will provide you with everything you need.
        </Typography>
        <Typography id={buildInBodyParsers.id} variant={'title'}>
          {buildInBodyParsers.display}
        </Typography>
        <Typography>
          Most typical web apps will not need to use custom body parsers, they can simply work with Play’s built in body parsers. These include parsers for JSON, XML, forms, as well as handling plain text bodies as Strings and byte bodies as <Highlighted>ByteString</Highlighted>.
        </Typography>

        <Typography variant="section">
          The default body parser
        </Typography>
        <Typography>
          The default body parser that’s used if you do not explicitly select a body parser will look at the incoming <Highlighted>Content-Type</Highlighted> header, and parses the body accordingly. So for example, a <Highlighted>Content-Type</Highlighted> of type <Highlighted>application/json</Highlighted> will be parsed as a <Highlighted>JsonNode</Highlighted>, while a <Highlighted>Content-Type</Highlighted> of <Highlighted>application/x-www-form-urlencoded</Highlighted> will be parsed as a <Highlighted>{`Map<String, String[]>`}</Highlighted>.
        </Typography>
        <Typography>
          The request body can be accessed through the body() method on Request, and is wrapped in a RequestBody object, which provides convenient accessors to the various types that the body could be. For example, to access a JSON body:
          <Code>
            {jsonBody}
          </Code>
          The following is a mapping of types supported by the default body parser:
          <ol>
            <li><Bold>text/plain</Bold>: String, accessible via asText().</li>
            <li><Bold>application/json</Bold>: com.fasterxml.jackson.databind.JsonNode, accessible via asJson().</li>
            <li><Bold>application/xml, text/xml or application/XXX+xml</Bold>: org.w3c.Document, accessible via asXml().</li>
            <li><Bold>application/x-www-form-urlencoded</Bold>: {`Map<String, String[]>`}, accessible via asFormUrlEncoded().</li>
            <li><Bold>multipart/form-data</Bold>: MultipartFormData, accessible via asMultipartFormData().</li>
            <li>Any other content type: RawBuffer, accessible via asRaw().</li>
          </ol>
          The default body parser tries to determine if the request has a body before it tries to parse. According to the HTTP spec, the presence of either the <Highlighted>Content-Length</Highlighted> or <Highlighted>Transfer-Encoding</Highlighted> header signals the presence of a body, so the parser will only parse if one of those headers is present, or on <Highlighted>FakeRequest</Highlighted> when a non-empty body has explicitly been set.
        </Typography>
        <Typography>
          If you would like to try to parse a body in all cases, you can use the AnyContent body parser, described below.
        </Typography>

        <Typography variant="section">
          Choosing an explicit body parser
        </Typography>
        <Typography>
          If you want to explicitly select a body parser, this can be done using the <Highlighted>@BodyParser.Of</Highlighted> annotation, for example:
          <ol>
            <li><Bold>Default</Bold>: The default body parser.</li>
            <li><Bold>AnyContent</Bold>: Like the default body parser, but will parse bodies of GET, HEAD and DELETE requests.</li>
            <li><Bold>Json</Bold>: Parses the body as JSON.</li>
            <li><Bold>TolerantJson</Bold>: Like Json, but does not validate that the Content-Type header is JSON.</li>
            <li><Bold>Xml</Bold>: Parses the body as XML.</li>
            <li><Bold>TolerantXml</Bold>: Like Xml, but does not validate that the Content-Type header is XML.</li>
            <li><Bold>Text</Bold>: Parses the body as a String.</li>
            <li><Bold>TolerantText</Bold>: Like Text, but does not validate that the Content-Type is text/plain.</li>
            <li><Bold>Bytes</Bold>: Parses the body as a ByteString.</li>
            <li><Bold>Raw</Bold>: Parses the body as a RawBuffer. This will attempt to store the body in memory, up to Play’s configured memory buffer size, but fallback to writing it out to a File if that’s exceeded.</li>
            <li><Bold>FormUrlEncoded</Bold>: Parses the body as a form.</li>
            <li><Bold>MultipartFormData</Bold>: Parses the body as a multipart form, storing file parts to files.</li>
            <li><Bold>Empty</Bold>: Does not parse the body, rather it ignores it.</li>
          </ol>
        </Typography>
        <Typography variant="section">
          Content length limits
        </Typography>
        <Typography>
          Most of the built in body parsers buffer the body in memory, and some buffer it on disk. If the buffering was unbounded, this would open up a potential vulnerability to malicious or careless use of the application. For this reason, Play has two configured buffer limits, one for in memory buffering, and one for disk buffering.
        </Typography>
        <Typography>
          The memory buffer limit is configured using <Highlighted>play.http.parser.maxMemoryBuffer</Highlighted>, and defaults to 100KB, while the disk buffer limit is configured using <Highlighted>play.http.parser.maxDiskBuffer</Highlighted>, and defaults to 10MB. These can both be configured in <Highlighted>application.conf</Highlighted>, for example, to increase the memory buffer limit to 256KB:
          <Code>
            {bufferMemorySetting}
          </Code>
          You can also limit the amount of memory used on a per action basis by writing a custom body parser, see below for details.
        </Typography>

        <Typography id={customBodyParsers.id} variant={'title'}>
          {customBodyParsers.display}
        </Typography>

        <Typography>
          A custom body parser can be made by implementing the BodyParser class. This class has one abstract method:
          <Code>
            {bodyParserAbstractMethod}
          </Code>
          As a first example, we’ll show how to compose an existing body parser. Let’s say you want to parse some incoming JSON into a class that you have defined, called <Highlighted>Item</Highlighted>.
        </Typography>
        <Typography>
          First we’ll define a new body parser that depends on the JSON body parser:
          <Code>
            {bodyParserMethod}
          </Code>
          Now, in our implementation of the <Highlighted>apply</Highlighted> method, we’ll invoke the JSON body parser, which will give us back the <Highlighted>{`Accumulator<ByteString, F.Either<Result, JsonNode>>`}</Highlighted> to consume the body. We can then map that like a promise, to convert the parsed <Highlighted>JsonNode</Highlighted> body to a User body. If the conversion fails, we return a <Highlighted>Left</Highlighted> of a Result saying what the error was:
          <Code>
            {implementation}
          </Code>
          The returned body will be wrapped in a <Highlighted>RequestBody</Highlighted>, and can be accessed using the <Highlighted>as</Highlighted> method:
          <Code>
            {customBodyParserUsage}
          </Code>
        </Typography>
        <Typography variant="section">
          Writing a custom max length body parser
        </Typography>
        <Typography>
          Another use case may be to define a body parser that uses a custom maximum length for buffering. Many of the built in Play body parsers are designed to be extended to allow overriding the buffer length in this way, for example, this is how the text body parser can be extended:
          <Code>
            {customContentLengthParser}
          </Code>
        </Typography>

        <Typography variant="section">
          Directing the body elsewhere
        </Typography>
        <Typography>
          So far we’ve shown extending and composing the existing body parsers. Sometimes you may not actually want to parse the body, you simply want to forward it elsewhere. For example, if you want to upload the request body to another service, you could do this by defining a custom body parser:
          <Code>
            {redirectingRequest}
          </Code>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(ManipulatingResponses)
