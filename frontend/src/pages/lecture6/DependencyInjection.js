/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Bold, Highlighted, Italic } from "presentations/Label";
import Code from "presentations/Code";

const styles = ({ typography }) => ({
  root: {},
})


const dependency = `libraryDependencies += guice`

const controllerDependency = `public class ApplicationController extends Controller {
	@Inject
	HttpExecutionContext ec;
	...
}`

const usingConstructor = `public class ApplicationController extends Controller {
	private final HttpExecutionContext ec;

	@Inject
	public ApplicationController (HttpExecutionContext ec) {
		this.ec = ec;
	}
	...
}`

const singletonExample = `import javax.inject.*;

@Singleton
public class CurrentSharePrice {
  private volatile int price;

  public void set(int p) {
    price = p;
  }

  public int get() {
    return price;
  }
}`

const cleaningUp = `import javax.inject.*;
import play.inject.ApplicationLifecycle;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

@Singleton
public class MessageQueueConnection {
  private final MessageQueue connection;

  @Inject
  public MessageQueueConnection(ApplicationLifecycle lifecycle) {
    connection = MessageQueue.connect();

    lifecycle.addStopHook(() -> {
      connection.stop();
      return CompletableFuture.completedFuture(null);
    });
  }

  // ...
}`

const coordintadCleanUp = `import javax.inject.*;
import akka.actor.CoordinatedShutdown;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

@Singleton
public class MessageQueueConnection {
  private final MessageQueue connection;

  @Inject
  public MessageQueueConnection(CoordinatedShutdown coordinatedShutdown) {
    connection = MessageQueue.connect();

    coordinatedShutdown.addTask(CoordinatedShutdown.PhaseBeforeActorSystemTerminate(), "shutting-down-message-queue", () -> {
        connection.stop();
        return CompletableFuture.completedFuture(Done.done());
    });
  }

  // ...
}`

const implementedByInterface = `import com.google.inject.ImplementedBy;

@ImplementedBy(EnglishHello.class)
public interface Hello {

  String sayHello(String name);
}`

const instanceImplementation = `public class EnglishHello implements Hello {

  public String sayHello(String name) {
    return "Hello " + name;
  }
}`

const guideModuleExample = `import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class Module extends AbstractModule {
  protected void configure() {

    bind(Hello.class).annotatedWith(Names.named("en")).to(EnglishHello.class);

    bind(Hello.class).annotatedWith(Names.named("de")).to(GermanHello.class);
  }
}`

const configuredBindings = `import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.typesafe.config.Config;
import play.Environment;

public class Module extends AbstractModule {

  private final Environment environment;
  private final Config config;

  public Module(Environment environment, Config config) {
    this.environment = environment;
    this.config = config;
  }

  protected void configure() {
    // Expect configuration like:
    // hello.en = "myapp.EnglishHello"
    // hello.de = "myapp.GermanHello"
    final Config helloConf = config.getConfig("hello");
    // Iterate through all the languages and bind the
    // class associated with that language. Use Play's
    // ClassLoader to load the classes.
    helloConf
        .entrySet()
        .forEach(
            entry -> {
              try {
                String name = entry.getKey();
                Class<? extends Hello> bindingClass =
                    environment
                        .classLoader()
                        .loadClass(entry.getValue().toString())
                        .asSubclass(Hello.class);
                bind(Hello.class).annotatedWith(Names.named(name)).to(bindingClass);
              } catch (ClassNotFoundException ex) {
                throw new RuntimeException(ex);
              }
            });
  }
}`

const eagerBinding = `import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

// A Module is needed to register bindings
public class Module extends AbstractModule {
  protected void configure() {

    // Bind the \`Hello\` interface to the \`EnglishHello\` implementation as eager singleton.
    bind(Hello.class).annotatedWith(Names.named("en")).to(EnglishHello.class).asEagerSingleton();

    bind(Hello.class).annotatedWith(Names.named("de")).to(GermanHello.class).asEagerSingleton();
  }
}`

const shutDownHooksEagerBinding = `import javax.inject.*;
import play.inject.ApplicationLifecycle;
import play.Environment;
import java.util.concurrent.CompletableFuture;

// This creates an \`ApplicationStart\` object once at start-up.
@Singleton
public class ApplicationStart {

  // Inject the application's Environment upon start-up and register hook(s) for shut-down.
  @Inject
  public ApplicationStart(ApplicationLifecycle lifecycle, Environment environment) {
    // Shut-down hook
    lifecycle.addStopHook(
        () -> {
          return CompletableFuture.completedFuture(null);
        });
    // ...
  }
}`

const applicationStartModule = `import com.google.inject.AbstractModule;

public class StartModule extends AbstractModule {
  protected void configure() {
    bind(ApplicationStart.class).asEagerSingleton();
  }
}`

const circularDependencies = `public class Foo {
  @Inject
  public Foo(Bar bar) {
    // ...
  }
}

public class Bar {
  @Inject
  public Bar(Baz baz) {
    // ...
  }
}

public class Baz {
  @Inject
  public Baz(Foo foo) {
    // ...
  }
}`

const circularProvider = `public class Foo {
  @Inject
  public Foo(Bar bar) {
    // ...
  }
}

public class Bar {
  @Inject
  public Bar(Baz baz) {
    // ...
  }
}

public class Baz {
  @Inject
  public Baz(Provider<Foo> fooProvider) {
    // ...
  }
}`
class DependencyInjection extends React.Component {
  render() {
    const { classes, section } = this.props
    const motivation = section.children[0]
    const declaringDependencies = section.children[1]
    const diControllers = section.children[2]
    const componentLifecycle = section.children[3]
    const singletons = section.children[4]
    const stoppingCleaningUp = section.children[5]
    const providingCustomBinding = section.children[6]
    const circular = section.children[7]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Section 6: {section.display}
          <Typography>
            Resources: <ol>
            <li><SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaDependencyInjection">https://www.playframework.com/documentation/2.8.x/JavaDependencyInjection</SimpleLink></li>
            <li><SimpleLink href="https://github.com/google/guice/wiki/Motivation">https://github.com/google/guice/wiki/Motivation</SimpleLink></li>
            <li><SimpleLink href="https://github.com/google/guice">https://github.com/google/guice</SimpleLink></li>
          </ol>
          </Typography>
          <Typography>
            This section introduces several ways to define generic action functionality.
          </Typography>
          <Divider />
        </Typography>
        <Typography>
          Dependency injection is a widely used design pattern that helps to separate your components’ behaviour from dependency resolution. Components declare their dependencies, usually as constructor parameters, and a dependency injection framework helps you wire together those components so you don’t have to do so manually.
        </Typography>
        <Typography>
          Out of the box, Play provides dependency injection support based on <SimpleLink href="https://jcp.org/en/jsr/detail?id=330">JSR 330</SimpleLink>. The default JSR 330 implementation that comes with Play is <SimpleLink href="https://github.com/google/guice">Guice</SimpleLink>, but other JSR 330 implementations can be plugged in. To enable the Play-provided Guice module, make sure you have guice in your library dependencies in build.sbt, e.g.:
          <Code>
            {dependency}
          </Code>
          The <SimpleLink href="https://github.com/google/guice/wiki">Guice wiki</SimpleLink> is a great resource for learning more about the features of Guice and DI design patterns in general.
        </Typography>
        <Typography id={motivation.id} variant={'title'}>
          {motivation.display}
        </Typography>
        <Typography>
          Dependency injection achieves several goals:
          <ol>
            <li>It allows you to easily bind different implementations for the same component. This is useful especially for testing, where you can manually instantiate components using mock dependencies or inject an alternate implementation.</li>
            <li>It allows you to avoid global static state. While static factories can achieve the first goal, you have to be careful to make sure your state is set up properly. In particular Play’s (now deprecated) static APIs require a running application, which makes testing less flexible. And having more than one instance available at a time makes it possible to run tests in parallel.</li>
          </ol>
        </Typography>

        <Typography variant="section">
          How it works
        </Typography>
        <Typography>
          Play provides a number of built-in components and declares them in modules such as its <SimpleLink href="https://www.playframework.com/documentation/2.8.x/api/scala/play/api/inject/BuiltinModule.html">BuiltinModule</SimpleLink>. These bindings describe everything that’s needed to create an instance of <Highlighted>Application</Highlighted>, including, by default, a router generated by the routes compiler that has your controllers injected into the constructor. These bindings can then be translated to work in Guice and other runtime DI frameworks.
        </Typography>
        <Typography>
          The Play team maintains the Guice module, which provides a GuiceApplicationLoader. That does the binding conversion for Guice, creates the Guice injector with those bindings, and requests an Application instance from the injector.
        </Typography>
        <Typography id={declaringDependencies.id} variant={'title'}>
          {declaringDependencies.display}
        </Typography>
        <Typography>
          If you have a component, such as a controller, and it requires some other components as dependencies, then this can be declared using the <Highlighted>@Inject</Highlighted> annotation. The @Inject annotation can be used on fields or on constructors. For example, to use field injection:
          <Code>
            {controllerDependency}
          </Code>
          Note that those are instance fields. It generally doesn’t make sense to inject a static field, since it would break encapsulation.
        </Typography>
        <Typography>
          To use constructor injection:
          <Code>
            {usingConstructor}
          </Code>
          Field injection is shorter, but Play recommends using constructor injection in your application. It is the most testable, since in a unit test you need to pass all the constructor arguments to create an instance of your class, and the compiler makes sure the dependencies are all there. It is also easy to understand what is going on, since there is no “magic” setting of fields going on. The DI framework is just automating the same constructor call you could write manually.
        </Typography>
        <Typography>
          Guice also has several other <SimpleLink href="https://github.com/google/guice/wiki/Injections">types of injections</SimpleLink> which may be useful in some cases. If you are migrating an application that uses statics, you may find its static injection support useful.
        </Typography>
        <Typography>
          Guice is able to automatically instantiate any class with an <Highlighted>@Inject</Highlighted> on its constructor without having to explicitly bind it. This feature is called <SimpleLink href="https://github.com/google/guice/wiki/JustInTimeBindings">just in time bindings</SimpleLink> is described in more detail in the Guice documentation. If you need to do something more sophisticated you can declare a custom binding as described below.
        </Typography>
        <Typography id={diControllers.id} variant={'title'}>
          {diControllers.display}
        </Typography>
        <Typography>
          Play’s routes compiler generates a router class that declares your controllers as dependencies in the constructor. This allows your controllers to be injected into the router.
        </Typography>
        <Typography>
          To enable the injected routes generator specifically, add the following to your build settings in build.sbt:
          <Code>
            routesGenerator := InjectedRoutesGenerator
          </Code>
          <Typography variant="italic">
            Which we already did
          </Typography>
          Prefixing the controller name with an <Highlighted>@</Highlighted> symbol takes on a special meaning: instead of the controller being injected directly, a Provider of the controller will be injected. This allows, for example, prototype controllers, as well as an option for breaking cyclic dependencies.
        </Typography>
        <Typography id={componentLifecycle.id} variant={'title'}>
          {componentLifecycle.display}
        </Typography>
        <Typography>
          The dependency injection system manages the lifecycle of injected components, creating them as needed and injecting them into other components. Here’s how component lifecycle works:
          <ol>
            <li><Bold>New instances are created every time a component is needed. If a component is used more than once, then, by default, multiple instances of the component will be created</Bold>. If you only want a single instance of a component then you need to mark it as a singleton.</li>
            <li><Bold>Instances are created lazily when they are needed. If a component is never used by another component, then it won’t be created at all</Bold>. This is usually what you want. For most components there’s no point creating them until they’re needed. However, in some cases you want components to be started up straight away or even if they’re not used by another component. For example, you might want to send a message to a remote system or warm up a cache when the application starts. You can force a component to be created eagerly by using an eager binding.</li>
            <li><Bold>Instances are not automatically cleaned up, beyond normal garbage collection</Bold>. Components will be garbage collected when they’re no longer referenced, but the framework won’t do anything special to shut down the component, like calling a close method. However, Play provides a special type of component, called the ApplicationLifecycle which lets you register components to shut down when the application stops.</li>
          </ol>
        </Typography>
        <Typography id={singletons.id} variant={'title'}>
          {singletons.display}
        </Typography>
        <Typography>
          Sometimes you may have a component that holds some state, such as a cache, or a connection to an external resource, or a component might be expensive to create. In these cases it may be important that there is only one instance of that component. This can be achieved using the <SimpleLink href="https://docs.oracle.com/javaee/7/api/javax/inject/Singleton.html">@Singleton</SimpleLink> annotation:
          <Code>
            {singletonExample}
          </Code>
        </Typography>
        <Typography id={stoppingCleaningUp.id} variant={'title'}>
          {stoppingCleaningUp.display}
        </Typography>
        <Typography>
          Some components may need to be cleaned up when Play shuts down, for example, to stop thread pools. Play provides an <Highlighted>ApplicationLifecycle</Highlighted> component that can be used to register hooks to stop your component when Play shuts down:
          <Code>
            {cleaningUp}
          </Code>
          The <Highlighted>ApplicationLifecycle</Highlighted> will stop all components in reverse order from when they were created. This means any components that you depend on can still safely be used in your components stop hook, since because you depend on them, they must have been created before your component was, and therefore won’t be stopped until after your component is stopped.
        </Typography>
        <Highlighted>
          Note: It’s very important to ensure that all components that register a stop hook are singletons. Any non singleton components that register stop hooks could potentially be a source of memory leaks, since a new stop hook will be registered each time the component is created.
        </Highlighted>
        <Typography>
          You can can also implement the cleanup logic using Coordinated Shutdown. Play uses Akka’s Coordinated Shutdown internally but it is also available for userland code. ApplicationLifecycle#stop is implemented as a Coordinated Shutdown task. The main difference is that ApplicationLifecycle#stop runs all stop hooks sequentially in a predictable order where <SimpleLink href="https://www.playframework.com/documentation/2.8.x/Shutdown">Coordinated Shutdown</SimpleLink> runs all tasks in the same phase in parallel which may be faster but unpredictable.
          <Code>
            {coordintadCleanUp}
          </Code>
        </Typography>
        <Typography id={providingCustomBinding.id} variant={'title'}>
          {providingCustomBinding.display}
        </Typography>
        <Typography>
          It is considered good practice to define an interface for a component, and have other classes depend on that interface, rather than the implementation of the component. By doing that, you can inject different implementations, for example you inject a mock implementation when testing your application.
        </Typography>
        <Typography>
          In this case, the DI system needs to know which implementation should be bound to that interface. The way we recommend that you declare this depends on whether you are writing a Play application as an end user of Play, or if you are writing library that other Play applications will consume.
        </Typography>
        <Typography variant="section">
          Play applications
        </Typography>
        <Typography>
          We recommend that Play applications use whatever mechanism is provided by the DI framework that the application is using. Although Play does provide a binding API, this API is somewhat limited, and will not allow you to take full advantage of the power of the framework you’re using.
        </Typography>
        <Typography>
          Since Play provides support for Guice out of the box, the examples below show how to provide bindings for Guice.
        </Typography>
        <Typography variant="section">
            Binding annotations
        </Typography>
        <Typography>
          The simplest way to bind an implementation to an interface is to use the Guice <Highlighted>@ImplementedBy</Highlighted> annotation. For example:
          <Code>
            {implementedByInterface}
          </Code>
          And the actual implementation:
          <Code>
            {instanceImplementation}
          </Code>
        </Typography>
        <Typography variant="section">
          Programmatic bindings
        </Typography>
        <Typography>
          In some more complex situations, you may want to provide more complex bindings, such as when you have multiple implementations of the one interface, which are qualified by <Highlighted>@Named</Highlighted> annotations. In these cases, you can implement a custom Guice <Highlighted>Module</Highlighted>:
        </Typography>
        <Code>
          {guideModuleExample}
        </Code>
        <Typography>
          If you call this module <Highlighted>Module</Highlighted> and place it in the root package, it will automatically be registered with Play. Alternatively, if you want to give it a different name or put it in a different package, you can register it with Play by appending its fully qualified class name to the <Highlighted>play.modules.enabled</Highlighted> list in <Highlighted>application.conf</Highlighted>:
        </Typography>
        <Code>
          play.modules.enabled += "modules.HelloModule"
        </Code>
        You can also disable the automatic registration of a module named Module in the root package by adding it to the disabled modules:
        <Code>
          play.modules.disabled += "Module"
        </Code>
        <Typography variant="section">
          Configurable bindings
        </Typography>
        <Typography>
          Sometimes you might want to read the Config (application.conf configuration) or use a ClassLoader when you configure Guice bindings. You can get access to these objects by adding them to your module’s constructor.
        </Typography>
        <Typography>
          In the example below, the Hello binding for each language is read from a configuration file. This allows new Hello bindings to be added by adding new settings in your application.conf file.
          <Code>
            {configuredBindings}
          </Code>
          <Highlighted>
            <Bold>Note</Bold>: In most cases, if you need to access Config when you create a component, you should inject the Config object into the component itself or into the component’s Provider. Then you can read the Config when you create the component. You usually don’t need to read Config when you create the bindings for the component.
          </Highlighted>
        </Typography>

        <Typography variant="section">
          Eager bindings
        </Typography>
        <Typography>
          In the code above, new EnglishHello and GermanHello objects will be created each time they are used. If you only want to create these objects once, perhaps because they’re expensive to create, then you should use the @Singleton annotation. If you want to create them once and also create them eagerly when the application starts up, rather than lazily when they are needed, then you can use Guice’s <SimpleLink href="https://github.com/google/guice/wiki/Scopes#eager-singletons">eager singleton binding</SimpleLink>.
          <Code>
            {eagerBinding}
          </Code>
          Eager singletons can be used to start up a service when an application starts. They are often combined with a shutdown hook so that the service can clean up its resources when the application stops.
          <Code>
            {shutDownHooksEagerBinding}
          </Code>
          <Code>
            {applicationStartModule}
          </Code>
        </Typography>
        <Typography id={circular.id} variant={'title'}>
          {circular.display}
        </Typography>
        <Typography>
          Circular dependencies happen when one of your components depends on another component that depends on the original component (either directly or indirectly). For example:
          <Code>
            {circularDependencies}
          </Code>
          In this case, <Italic>Foo</Italic> depends on <Italic>Bar</Italic>, which depends on <Italic>Baz</Italic>, which depends on <Italic>Foo</Italic>. So you won’t be able to instantiate any of these classes. You can work around this problem by using a <Highlighted>Provider</Highlighted>:
          <Code>
            {circularProvider}
          </Code>
          Note that if you’re using constructor injection it will be much more clear when you have a circular dependency, since it will be impossible to instantiate the component manually.
        </Typography>
        <Typography>
          Generally, circular dependencies can be resolved by breaking up your components in a more atomic way, or finding a more specific component to depend on. A common problem is a dependency on Application. When your component depends on Application it’s saying that it needs a complete application to do its job; typically that’s not the case. Your dependencies should be on more specific components (e.g. Environment) that have the specific functionality you need. As a last resort you can work around the problem by injecting a <Highlighted>{`Provider<Application>`}</Highlighted>.
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(DependencyInjection)
