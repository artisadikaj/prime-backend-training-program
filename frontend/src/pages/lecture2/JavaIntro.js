/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import Code from "presentations/Code";
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import SimpleLink from "presentations/rows/SimpleLink";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { Italic, Bold } from "presentations/Label";
import JavaFeaturesImage from 'assets/images/lecture2/java-features.png'
import PrimitiveDataTypesImage from 'assets/images/lecture2/primitive-data-types.png'
import classNames from 'classnames'

const styles = ({ typography }) => ({
  root: {},
  horizontal: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    alignContent: 'flex-start',
    width: '100%',
    '& $vertical': {
    },
    '& $big': {
    }
  },
  vertical: {
    display: 'flex',
    flexFlow: 'column wrap',
    alignItems: 'flex-start',
    alignContent: 'flex-start'
  },
  big: {}
})

const simpleClassCode = `class Simple{  
  public static void main(String args[]){  
    System.out.println("Hello Java");  
  }  
}`
const stronglyTypedCode = `String name ="Something";
name = 2 // error`

const passByValueCode = `
@AllArgsConstructor // Lombok Library Magic
@Data // More Lombok Magic
public class Dog {
  private String name;
}
  
public void passByValue() {
  Dog aDog = new Dog("Max");
  Dog oldDog = aDog;

  foo(aDog);
  // when foo(...) returns, the name of the dog has been changed to "Rock"
  Logger.of(this.getClass()).debug("Is it Rock {}", aDog.getName().equals("Rock")); // true
  Logger.of(this.getClass()).debug("Is it Rock {}", oldDog.getName().equals("Rock")); // true
  // but it is still the same dog:
  Logger.of(this.getClass()).debug("Dogs the same {} at count {}", aDog == oldDog); // true

  int i = 5;
  this.manipulate(i);
  Logger.of(this.getClass()).debug("What happened to i {}", i);
}

public void foo(Dog d) {
  d.getName().equals("Max"); // true
  // this changes the name of d to be "Rock"
  d.setName("Rock");
  d = new Dog();
}

public void manipulate (int i) {
  i = 10;
}`

const variableCode = `int data=50; //Here data is variable`

class JavaIntro extends React.Component {
  render() {
    const { classes, section } = this.props
    let intro = section.children[0]
    let syntax = section.children[1]
    let variables = section.children[2]
    let dataTypes = section.children[3]
    let stronglyTyped = section.children[4]
    let passByValue = section.children[5]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Typography variant={'p'}>
            In this section we are introducing the Java Programing Language and its features. The content is inspired from <SimpleLink href="https://www.javatpoint.com/features-of-java">Java Point</SimpleLink> and other sources
          </Typography>
          <Divider />
        </Typography>
        <Typography id={intro.id} variant={'title'}>
          {intro.display}
        </Typography>
        <Typography variant={'p'}>
          Java is a <Bold>programming language</Bold> and a <Bold>platform</Bold>. Java is a high level, robust, object-oriented and secure programming language.
        </Typography>
        <Typography>
          Java was developed by Sun Microsystems (which is now the subsidiary of Oracle) in the year 1995. James Gosling is known as the father of Java. Before Java, its name was Oak. Since Oak was already a registered company, so James Gosling and his team changed the Oak name to Java.
        </Typography>
        <Typography>
          The primary objective of Java programming language creation was to make it portable, simple and secure programming language. And the following features are why:
          <div className={classes.horizontal}>
            <div className={classes.vertical}>
              <img src={JavaFeaturesImage} />
            </div>
            <div className={classes.vertical}>
              <ol>
                <li>Simple</li>
                <li>Object-Oriented</li>
                <li>Portable</li>
                <li>Platform independent</li>
                <li>Secured</li>
                <li>Robust</li>
                <li>Architecture neutral</li>
                <li>Interpreted</li>
                <li>High Performance</li>
                <li>Multithreaded</li>
                <li>Distributed</li>
                <li>Dynamic</li>
              </ol>
            </div>
          </div>
        </Typography>
        <Typography>
          Java is a general-purpose programming language that is class-based, object-oriented, and designed to have as few implementation dependencies as possible. It is intended to let application developers write once, run anywhere (WORA), meaning that compiled Java code can run on all platforms that support Java without the need for recompilation. Java applications are typically compiled to bytecode that can run on any Java virtual machine (JVM) regardless of the underlying computer architecture. The syntax of Java is similar to C and C++, but it has fewer low-level facilities than either of them
        </Typography>
        <Typography>
          Here is a brief summary of the Java Features:
          <ol>
            <li><b>Robust</b>:</li>
            <ul>
              <li>It uses strong memory management.</li>
              <li>There is a lack of pointers that avoids security problems.</li>
              <li>There is automatic garbage collection in java which runs on the Java Virtual Machine to get rid of objects which are not being used by a Java application anymore.</li>
              <li>There are exception handling and the type checking mechanism in Java. All these points make Java robust.</li>
            </ul>
            <li><b>Architecture-neutral</b>: There are no implementation dependent features</li>
            <li><b>Portable</b>: Java is portable because it facilitates you to carry the Java bytecode to any platform. It doesn't require any implementation.</li>
            <li><b>High-performance</b>: Java is faster than other traditional interpreted programming languages because Java bytecode is "close" to native code.</li>
            <li><b>Distributed</b>: Java is distributed because it facilitates users to create distributed applications in Java. RMI and EJB are used for creating distributed applications. This feature of Java makes us able to access files by calling the methods from any machine on the internet.</li>
            <li><b>Multi-threaded</b>: Concurrency and multi tasking, backround jobs, shared memory between threads and lots of more all possible. We can write Java programs that deal with many tasks at once by defining multiple threads.
            </li>
            <li><b>Dynamic</b>: Java is a dynamic language. It supports dynamic loading of classes. It means classes are loaded on demand. It also supports functions from its native languages, i.e., C and C++.</li>
          </ol>
        </Typography>
        <Typography id={syntax.id} variant={'title'}>
          {syntax.display}
        </Typography>
        <Typography>
          Here is the simplest Java Code ever:
          <Code>
            {simpleClassCode}
          </Code>
          <ol>
            <li><b>class</b> keyword is used to declare a class in java.</li>
            <li><b>public</b> keyword is an access modifier which represents visibility. It means it is visible to all.</li>
            <li><b>static</b> is a keyword. If we declare any method as static, it is known as the static method. The core advantage of the static method is that there is no need to create an object to invoke the static method. The main method is executed by the JVM, so it doesn't require to create an object to invoke the main method. So it saves memory.</li>
            <li><b>void</b> is the return type of the method. It means it doesn't return any value.</li>
            <li><b>main</b> represents the starting point of the program.</li>
            <li><b>String[]</b> args is used for command line argument. We will learn it later.</li>
            <li><b>System.out.println()</b> is used to print statement. Here, System is a class, out is the object of PrintStream class, println() is the method of PrintStream class</li>
          </ol>
        </Typography>
        <Typography fontStyle={'italic'}>
          A bonus guide on Java Naming Conventions: <SimpleLink href="https://www.javatpoint.com/java-naming-conventions">https://www.javatpoint.com/java-naming-conventions</SimpleLink>
        </Typography>
        <Typography id={variables.id} variant={'title'}>
          {variables.display}
        </Typography>
        <Typography>
          A variable is a container which holds the value while the Java program is executed. A variable is assigned with a data type. Variable is name of reserved area allocated in memory. In other words, it is a name of memory location. It is a combination of "vary + able" that means its value can be changed.
          <Code>
            {variableCode}
          </Code>
          There are three types of variables in Java:
          <ol>
            <li>Local Variable: A variable declared inside the body of the method is called local variable. You can use this variable only within that method and the other methods in the class aren't even aware that the variable exists.</li>
            <li>Instance Variable: A variable declared inside the class but outside the body of the method, is called instance variable. It is not declared as static.</li>
            <li>Static Variable: A variable which is declared as static is called static variable.</li>
          </ol>
        </Typography>
        <Typography id={dataTypes.id} variant={'title'}>
          {dataTypes.display}
        </Typography>
        <Typography>
          Data types specify the different sizes and values that can be stored in the variable. There are two types of data types in Java:
          <ol>
            <li><b>Primitive</b> data types: The primitive data types include boolean, char, byte, short, int, long, float and double.</li>
            <li><b>Non-primitive</b> data types: The non-primitive data types include Classes, Interfaces, and Arrays</li>
          </ol>
          In other words:
          <ol>
            <li><b>Primitive</b> data types: Are pre-defined by the programming language. The size and type of variable values are specified, and it has no additional methods.</li>
            <li><b>Non-primitive</b> data types: These data types are not actually defined by the programming language but are created by the programmer. They are also called “reference variables” or “object references” since they reference a memory location which stores the data.</li>
          </ol>
          <Typography variant={'section'}>
            Primitives
          </Typography>

          <Typography>
            Data types in Java are classified into 4 aspects as <Bold>int</Bold>, <Bold>float</Bold>, <Bold>character</Bold> and <Bold>boolean</Bold>. But, in general, there are 8 data types. They are as follows:
          </Typography>
          <div className={classes.horizontal}>
            <div className={classNames(classes.vertical, classes.big)}>
              <img style={{ maxWidth: 1280 }} src={PrimitiveDataTypesImage}/>
            </div>
            <div className={classes.vertical}>
              <Typography>
                <ol>
                  <li><b>boolean</b> data type</li>
                  <li><b>byte</b> data type</li>
                  <li><b>char</b> data type</li>
                  <li><b>short</b> data type</li>
                  <li><b>int</b> data type</li>
                  <li><b>long</b> data type</li>
                  <li><b>float</b> data type</li>
                  <li><b>double</b> data type</li>
                </ol>
              </Typography>
            </div>
          </div>
          For more information refer to: <SimpleLink href="https://www.edureka.co/blog/data-types-in-java/">https://www.edureka.co/blog/data-types-in-java/</SimpleLink> or <SimpleLink href="https://www.javatpoint.com/java-data-types">https://www.javatpoint.com/java-data-types</SimpleLink>
        </Typography>
        <Typography id={stronglyTyped.id} variant={'title'}>
          {stronglyTyped.display}
        </Typography>
        <Typography>
          Java is a strongly typed programming language because every variable must be declared with a data type. A variable cannot start off life without knowing the range of values it can hold, and once it is declared, the data type of the variable cannot change.
          <Code>
            {stronglyTypedCode}
          </Code>
        </Typography>

        <Typography id={passByValue.id} variant='title'>
          {passByValue.display}
        </Typography>
        <Typography>
          First lets see a definition of the difference between 'Pass by Value' and 'Pass by Reference'
          <ol>
            <li><b>Pass-by-value:</b> The actual parameter (or argument expression) is fully evaluated and the resulting value is copied into a location being used to hold the formal parameter's value during method/function execution. That location is typically a chunk of memory on the runtime stack for the application (which is how Java handles it), but other languages could choose parameter storage differently.</li>
            <li><b>Pass-by-reference:</b> The formal parameter merely acts as an alias for the actual parameter. Anytime the method/function uses the formal parameter (for reading or writing), it is actually using the actual parameter.</li>
          </ol>
          Java is strictly <Bold>pass-by-value</Bold>. In that sense Object references are passed by value and none primitives are also passed by value.
          Here is an example to demonstrate that:
          <Code>
            {passByValueCode}
          </Code>
          If you want more details on this follow this article: <SimpleLink href="http://www.javadude.com/articles/passbyvalue.htm">http://www.javadude.com/articles/passbyvalue.htm</SimpleLink>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(JavaIntro)
